`This is where you define your skills intents and entities.` \
`See https://rasa.com/docs/rasa/nlu/training-data-format/ for full explanation.` \
`You can make comments with the `` markup signs.` 

## intent:get_riddle
`This is a simple intent without entities.` \
`The name should be in snake_case_style.` \
`It will later be prefixed with your skill name.` \
`Only use a minus - sign for the list, no asterisk or plus.`
- Je veux une énigme
- Donne-moi quelque chose à deviner
- J'ai besoin de réfléchir à quelque chose
- J'aimerais avoir une nouvelle énigme
- Nouvelle énigme
- Donne-moi une énigme
- Un nouvelle énigme, s'il te plaît
- Donne-moi une énigme
- Nouvelle énigme
- Soumets-moi une énigme

## lookup:riddle_answers
`If you want to use entities, create a lookup file.` \
`In the lookup file write one example per line, nothing else.` \
`The files name has to be the same as the lookup name.`
riddle_answers.txt

## intent:check_riddle
`This is an intent with entities.` \
`The synoniym definition in intents, like in rasa, is not supported here, use an extra group.` \
`Every entity example used here also has to be in the lookup file or it will be left out.` \
`Be careful if you use more than one entity in a sentence, for the language model every possible combination is generated.`
- La réponse est la [neige](riddle_answers)
- La solution est la [poupée](riddle_answers)
- La réponse à l'énigme est le [feu](riddle_answers)
- La solution de l'énigme est le [arbre](riddle_answers)
- la réponse est [arc-en-ciel](riddle_answers)
- La solution de l'énigme est [étincelle](riddle_answers)
- Pour l'énigme, la bonne réponse est [cercueil](riddle_answers)
- La réponse est les [ciseaux](riddle_answers)
- La réponse est l'[arbre](riddle_answers)
