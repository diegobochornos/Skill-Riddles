`This is where you define your skills intents and entities.` \
`See https://rasa.com/docs/rasa/nlu/training-data-format/ for full explanation.` \
`You can make comments with the `` markup signs.` 

## intent:get_riddle
`This is a simple intent without entities.` \
`The name should be in snake_case_style.` \
`It will later be prefixed with your skill name.` \
`Only use a minus - sign for the list, no asterisk or plus.`
- I want a riddle
- Let me guess something
- I need something to think about
- I'd like a new puzzle
- New Riddle
- Give me a riddle
- A new riddle please
- Give me a riddle
- New riddle
- Tell me a new riddle

## lookup:riddle_answers
`If you want to use entities, create a lookup file.` \
`In the lookup file write one example per line, nothing else.` \
`The files name has to be the same as the lookup name.`
riddle_answers.txt

## intent:check_riddle
`This is an intent with entities.` \
`The synoniym definition in intents, like in rasa, is not supported here, use an extra group.` \
`Every entity example used here also has to be in the lookup file or it will be left out.` \
`Be careful if you use more than one entity in a sentence, for the language model every possible combination is generated.`
- The answer is [snow](riddle_answers)
- The solution is [doll](riddle_answers)
- The answer to the riddle is [fire](riddle_answers)
- The solution is a [tree](riddle_answers)
- The answer of the riddle is [bread](riddle_answers)
- Answer is [rainbow](riddle_answers)
- The solution of the riddle is a [car](riddle_answers)
- For the open riddle the correct solution is [coffin](riddle_answers)
