`This is where you define your skills intents and entities.` \
`See https://rasa.com/docs/rasa/nlu/training-data-format/ for full explanation.` \
`You can make comments with the `` markup signs.` 

## intent:get_riddle
`This is a simple intent without entities.` \
`The name should be in snake_case_style.` \
`It will later be prefixed with your skill name.` \
`Only use a minus - sign for the list, no asterisk or plus.`
- Ich möchte ein Rätsel
- Lass mich was rätseln
- Ich brauch was zum Nachdenken
- Ich hätte gerne ein neues Rätsel
- Neues Riddle
- Gib mir ein Riddle
- Ein neues Rätsel bitte
- Gib mir ein Rätsel
- Neues Rätsel
- Erzähle mir ein Rätsel

## lookup:riddle_answers
`If you want to use entities, create a lookup file.` \
`In the lookup file write one example per line, nothing else.` \
`The files name has to be the same as the lookup name.`
riddle_answers.txt

## intent:check_riddle
`This is an intent with entities.` \
`The synoniym definition in intents, like in rasa, is not supported here, use an extra group.` \
`Every entity example used here also has to be in the lookup file or it will be left out.` \
`Be careful if you use more than one entity in a sentence, for the language model every possible combination is generated.`
- Die Antwort ist [Schnee](riddle_answers)
- Die Lösung ist [Puppe](riddle_answers)
- Die Antwort auf das Rätsel ist [Feuer](riddle_answers)
- Des Rätsels Lösung ist [Baum](riddle_answers)
- Die Antwort des Rätsels ist [Brot](riddle_answers)
- Antwort ist [Regenbogen](riddle_answers)
- Die Lösung des Rätels ist [Auto](riddle_answers)
- Für das offene Rätsel ist die korrekte Antwort [Sarg](riddle_answers)
